package com.jualginjal.pencarijasa.helper;

/**
 * Created by nasrulhamid on 12/05/17.
 */

public class ReviewSumModel {
    private int jid, x1, x2, x3, x4, x5;
    private float arate;

    public ReviewSumModel(int jid, int x1, int x2, int x3, int x4, int x5, float arate) {
        this.jid = jid;
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
        this.x4 = x4;
        this.x5 = x5;
        this.arate = arate;
    }

    public int getJid() {
        return jid;
    }
    public int getX1() {
        return x1;
    }
    public int getX2() {
        return x2;
    }
    public int getX3() {
        return x3;
    }
    public int getX4() {
        return x4;
    }
    public int getX5() {
        return x5;
    }
    public float getArate() {
        return arate;
    }

}

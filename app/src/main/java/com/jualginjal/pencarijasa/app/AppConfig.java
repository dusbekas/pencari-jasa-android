package com.jualginjal.pencarijasa.app;

public class AppConfig {

//	public static String IP_ADDRESS = "http://192.168.122.1/~nasrulhamid/ajasa/";
	public static String IP_ADDRESS = "http://alpukat.nasrulhamid.web.id/";

	// Server user login url
	public static String URL_LOGIN = IP_ADDRESS+"backend/login.php";

	// Server user register url
	public static String URL_REGISTER = IP_ADDRESS+"backend/register.php";

	// Server category url
	public static String URL_CATEGORY = IP_ADDRESS+"backend/category_view.php";
	public static String URL_SERVICE_ALL = IP_ADDRESS+"backend/service_all.php";
	public static String URL_SERVICE_CARI = IP_ADDRESS+"backend/service_cari.php";

	// Server category url
	public static String URL_SERVICE_CATEGORY = IP_ADDRESS+"backend/service_view.php";

	// Server category url
	public static String URL_SERVICE_DETAIL = IP_ADDRESS+"backend/service_detail.php";

	public static String URL_SCRIPT = IP_ADDRESS+"backend/script.php";
	public static String URL_REVIEW = IP_ADDRESS+"backend/review_model.php";
	public static String URL_REVIEW_SUMM = IP_ADDRESS+"backend/review_summary.php";
	public static String URL_REVIEW_SUBMIT = IP_ADDRESS+"backend/review_submit.php";

	// Server Tambah Jasa
	public static String URL_TAMBAHJASA = "http://alpukat.nasrulhamid.web.id/backend/tambahJasa.php";
	public static String URL_KATEGORI = "http://alpukat.nasrulhamid.web.id/backend/kategori.php";
}

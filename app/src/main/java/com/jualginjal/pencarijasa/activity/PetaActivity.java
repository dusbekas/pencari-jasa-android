package com.jualginjal.pencarijasa.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.jualginjal.pencarijasa.R;

import java.util.List;
import java.util.Locale;

public class PetaActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String[] INITIAL_PERMS={
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.READ_CONTACTS
    };
    private static final int INITIAL_REQUEST=1337;
    private static final int CAMERA_REQUEST=INITIAL_REQUEST+1;
    private static final int CONTACTS_REQUEST=INITIAL_REQUEST+2;
    private static final int LOCATION_REQUEST=INITIAL_REQUEST+3;


    private GoogleMap mMap;
    private TextView markerText;
    private LinearLayout markerLayout;
    private LatLng center;

//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    protected void onStart() {
//        super.onStart();
//        if (!canAccessLocation()) {
//            requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
//        }
//    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peta);

        markerText = (TextView) findViewById(R.id.locationMarkertext);
        markerLayout = (LinearLayout) findViewById(R.id.locationMarker);

        try {
            // Loading map
            initilizeMap();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Button btnLokasi = (Button)findViewById(R.id.btnLokasi);
        final TextView txtLokasi = (TextView) findViewById(R.id.txtLokasi);

        btnLokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundleTambah = getIntent().getExtras();
                Bundle bundle = new Bundle();
                bundle.putString("namaJasa", bundleTambah.getString("namaJasa"));
                bundle.putString("kategori", bundleTambah.getString("kategori"));
                bundle.putString("deskripsi", bundleTambah.getString("deskripsi"));
                if(bundleTambah.getString("gambar")!=null)
                    bundle.putString("gambar", bundleTambah.getString("gambar"));
                bundle.putString("alamat", txtLokasi.getText().toString());
                bundle.putString("latitude", String.valueOf(center.latitude));
                bundle.putString("longitude", String.valueOf(center.longitude));
                bundle.putString("noTelp",  bundleTambah.getString("noTelp"));
                bundle.putString("email", bundleTambah.getString("email"));
                Intent intent = new Intent(PetaActivity.this, TambahJasaActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    private void initilizeMap() {
        if (mMap == null) {
            ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMapAsync(this);

            // check if map is created successfully or not
            if (mMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            //Not in api-23, no need to prompt
            mMap.setMyLocationEnabled(true);
        }

        LatLng mLatLng = new LatLng(-6.594428, 106.805581);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(mLatLng).zoom(15f).build();

        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
        mMap.clear();

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                center = mMap.getCameraPosition().target;
                markerText.setText(" Set your Location ");
                mMap.clear();
                markerLayout.setVisibility(View.VISIBLE);

                TextView txtLokasi = (TextView) findViewById(R.id.txtLokasi);
                String alamat = getCompleteAddressString(center.latitude, center.longitude);
                txtLokasi.setText(alamat);
            }
        });
    }

    @SuppressLint("LongLogTag")
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    if(i==returnedAddress.getMaxAddressLineIndex()-1)
                        strReturnedAddress.append(returnedAddress.getAddressLine(i));
                    else
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(", ");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current loction address", "" + strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean canAccessLocation() {
        return(hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

}
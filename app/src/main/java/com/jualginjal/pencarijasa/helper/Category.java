package com.jualginjal.pencarijasa.helper;

/**
 * Created by Hut Hut on 5/21/2017.
 */

public class Category {

    private int id;
    private String kategori;

    public Category(){}

    public Category(int id, String kategori){
        this.id = id;
        this.kategori = kategori;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setName(String kategori){
        this.kategori = kategori;
    }

    public int getId(){
        return this.id;
    }

    public String getName(){
        return this.kategori;
    }

}

package com.jualginjal.pencarijasa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.jualginjal.pencarijasa.R;

public class JasaSayaActivity extends AppCompatActivity {
    private Button mButtonTambahJasa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jasa_saya);

        mButtonTambahJasa = (Button) findViewById(R.id.btnTambahJasa);

        mButtonTambahJasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JasaSayaActivity.this, TambahJasaActivity.class));
            }
        });
    }

}

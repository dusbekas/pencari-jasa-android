package com.jualginjal.pencarijasa.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.jualginjal.pencarijasa.R;
import com.jualginjal.pencarijasa.app.AppConfig;
import com.jualginjal.pencarijasa.helper.ReviewSumModel;
import com.jualginjal.pencarijasa.helper.ServerImageParseAdapter;
import com.jualginjal.pencarijasa.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class DetailJasaActivity extends AppCompatActivity {

    private TextView txtName;
    private TextView txtDescription;
    private TextView txtAddress;
    private TextView txtPhone;
    private TextView txtEmail;
    private RatingBar rbScore;
    private TextView txtScore;
    private Button btnViewLocation;
    private Button btnLihatUlasan;
    private NetworkImageView imgService;
    protected ImageLoader imageLoader;

    String name, description, address, phone, email, imageUrl;
    private Button btnEmail;
    private Button btnSms;
    private Button btnCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_jasa);

        //Get intent
        Bundle bundle = getIntent().getExtras();
        final String idJasa = bundle.getString("idJasa");

        txtName = (TextView) findViewById(R.id.txt_detail_jasa_name);
        txtDescription = (TextView) findViewById(R.id.txt_detail_jasa_description);
        txtAddress = (TextView) findViewById(R.id.txt_detail_jasa_address);
        txtPhone = (TextView) findViewById(R.id.txt_detail_jasa_phone);
        txtEmail = (TextView) findViewById(R.id.txt_detail_jasa_email);
        imgService = (NetworkImageView) findViewById(R.id.img_detail_jasa) ;
        rbScore = (RatingBar) findViewById(R.id.rbScoreJasa);
        btnLihatUlasan = (Button) findViewById(R.id.btnLihatUlasan);
        btnEmail = (Button) findViewById(R.id.btn_mail);
        btnSms = (Button) findViewById(R.id.btn_sms);
        btnCall = (Button) findViewById(R.id.btn_call);

        getDetailJasa(idJasa);
        load_review_summ(Integer.parseInt(idJasa));

        btnLihatUlasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailJasaActivity.this, ReviewActivity.class);
                intent.putExtra("jid", idJasa);
                startActivity(intent);
            }
        });

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });

        btnSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                composeSmsMessage();
            }
        });

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialPhoneNumber();
            }
        });


    }

    private void sendEmail() {
        Log.i("Send email", "");
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        //emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Tanya: "+name);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            //finish();
            Log.i("Finished sending email", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(DetailJasaActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void composeSmsMessage() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:"+phone));  // This ensures only SMS apps respond
        intent.putExtra("sms_body", "");
        startActivity(Intent.createChooser(intent, "Send sms..."));
    }

    private void dialPhoneNumber() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    private void getDetailJasa (String idJasa) {
        Volley.newRequestQueue(DetailJasaActivity.this).add(
                new DetailJasaActivity.CustomJsonRequest(Request.Method.POST, idJasa,
                        AppConfig.URL_SERVICE_DETAIL,null, new Response.Listener<JSONArray>(){


                            @Override
                            public void onResponse(JSONArray response) {

                                try {
                                    JSONObject jasa = (JSONObject)response.get(0);

                                    name = jasa.getString("service_name");
                                    description = jasa.getString("description");
                                    address = jasa.getString("address");
                                    phone = jasa.getString("phone");
                                    email = jasa.getString("email");
                                    imageUrl = AppConfig.IP_ADDRESS + jasa.getString("image_url");


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                txtName.setText(name);
                                txtDescription.setText(description);
                                txtAddress.setText(address);
                                txtPhone.setText(phone);
                                txtEmail.setText(email);

                                imageLoader = ServerImageParseAdapter.
                                        getInstance(DetailJasaActivity.this.getApplicationContext()).getImageLoader();
                                imageLoader.get(imageUrl, ImageLoader.getImageListener(
                                        imgService,R.mipmap.ic_launcher,android.R.drawable.ic_dialog_alert));
                                imgService.setImageUrl(imageUrl,imageLoader);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Error: " + error.getMessage());
                    }
                }) {
                    @Override
                    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                        try {
                            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(
                                    response.headers));
                            return Response.success(new JSONArray(jsonString),HttpHeaderParser.parseCacheHeaders(
                                    response
                            ));
                        } catch (UnsupportedEncodingException e) {
                            return Response.error(new ParseError(e));
                        } catch (JSONException je) {
                            return Response.error(new ParseError(je));
                        }
                    }
                }

        );
    }

    public static class CustomJsonRequest extends Request {

        Map<String, String> params;
        private Response.Listener listener;

        public CustomJsonRequest(int requestMethod, String service_id, String url, Map<String, String> params,
                                 Response.Listener responseListener, Response.ErrorListener errorListener) {

            super(requestMethod, url, errorListener);

            params = new HashMap<String, String>();
            params.put("service_id", service_id);
            this.params = params;
            this.listener = responseListener;
        }

        @Override
        protected void deliverResponse(Object response) {
            listener.onResponse(response);

        }

        @Override
        public Map<String, String> getParams() throws AuthFailureError {

            return params;
        }

        @Override
        protected Response parseNetworkResponse(NetworkResponse response) {
            try {
                String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                return Response.success(new JSONObject(jsonString),
                        HttpHeaderParser.parseCacheHeaders(response));
            } catch (UnsupportedEncodingException e) {
                return Response.error(new ParseError(e));
            } catch (JSONException je) {
                return Response.error(new ParseError(je));
            }
        }
    }

    private void load_review_summ (final int id) {
        AsyncTask<Integer, Void, Void> task  = new AsyncTask<Integer, Void, Void>() {
            String strRateAll="", strAchievements="";
            JSONArray array;

            @SuppressLint("DefaultLocale")
            @Override
            protected Void doInBackground(Integer... integers) {
                OkHttpClient client = new OkHttpClient();
                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("id", String.valueOf(id))
                        .build();

                okhttp3.Request request = new okhttp3.Request.Builder()
                        .url(AppConfig.URL_REVIEW_SUMM)
                        .method("POST", RequestBody.create(null, new byte[0]))
                        .post(requestBody)
                        .build();

                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    array = new JSONArray(response.body().string());

                    JSONObject object = array.getJSONObject(0);
                    ReviewSumModel data = new ReviewSumModel(
                            object.getInt("jid"),
                            object.getInt("x1"),
                            object.getInt("x2"),
                            object.getInt("x3"),
                            object.getInt("x4"),
                            object.getInt("x5"),
                            (float) object.getDouble("arate")
                    );

                    strRateAll = String.format("%.1f", data.getArate());
                    strAchievements = (
                            "*1 = " + data.getX1() + "\n" +
                                    "*2 = " + data.getX2() + "\n" +
                                    "*3 = " + data.getX3() + "\n" +
                                    "*4 = " + data.getX4() + "\n" +
                                    "*5 = " + data.getX5()
                    );

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }

                return null;
            }

            @SuppressLint("SetTextI18n")
            @Override
            protected void onPostExecute(Void aVoid) {
                rbScore = (RatingBar) findViewById(R.id.rbScoreJasa);
                if (strRateAll.trim().length()==0) strRateAll="0.0";
                rbScore.setRating(Float.parseFloat(strRateAll));

            }

        };
        task.execute(id);
    }
}

package com.jualginjal.pencarijasa.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;

import com.android.volley.toolbox.Volley;
import com.jualginjal.pencarijasa.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.jualginjal.pencarijasa.helper.Category;
import com.jualginjal.pencarijasa.helper.SQLiteHandler;
import com.jualginjal.pencarijasa.helper.SessionManager;

public class TambahJasaActivity extends AppCompatActivity{

    private Spinner spinner;
    private ArrayList<Category> listKategori;
    private AlertDialog.Builder builder;
    private static final int GET_FROM_GALLERY = 3;
    private int cekKategori;
    private String latitude, longitude, StrGambar=null;
    private Bitmap bitmap = null;
    private SessionManager session;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_jasa);
        listKategori = new ArrayList<Category>();
        builder = new AlertDialog.Builder(this);

        spinner = (Spinner) findViewById(R.id.spnKategori);
        GetCategories();
        SimpanJasa();

        Button btnCari = (Button)findViewById(R.id.btnCari);
        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText namaJasa = (EditText) findViewById(R.id.namaJasa);
                EditText deskripsi = (EditText) findViewById(R.id.deskripsi);
                EditText noTelp = (EditText) findViewById(R.id.noTelp);
                EditText email = (EditText) findViewById(R.id.email);

                Bundle bundle = new Bundle();
                bundle.putString("namaJasa", namaJasa.getText().toString());
                bundle.putString("kategori", Integer.toString(cekKategori));
                bundle.putString("deskripsi", deskripsi.getText().toString());
                bundle.putString("noTelp",  noTelp.getText().toString());
                bundle.putString("email", email.getText().toString());
                if(StrGambar!=null)
                    bundle.putString("gambar", StrGambar);
                Intent intent = new Intent(TambahJasaActivity.this, PetaActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                for(int i=0;i<listKategori.size();i++){
                    if(parent.getItemAtPosition(position).toString().equals(listKategori.get(i).getName())){
                        cekKategori = listKategori.get(i).getId();
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });

        cek();
    }

    private static final String[] INITIAL_PERMS={
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.READ_CONTACTS
    };
    private static final int INITIAL_REQUEST=1337;

//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    protected void onStart() {
//        super.onStart();
//        if (!canAccessLocation()) {
//            //requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
//        }
//    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean canAccessLocation() {
        return(hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

    public void cek() {

        TextView alamat = (TextView) findViewById(R.id.alamatJasa);
        EditText namaJasa = (EditText) findViewById(R.id.namaJasa);
        //Spinner  kategori = (Spinner) findViewById(R.id.spnKategori);
        EditText deskripsi = (EditText) findViewById(R.id.deskripsi);
        EditText noTelp = (EditText) findViewById(R.id.noTelp);
        EditText email = (EditText) findViewById(R.id.email);

        if(getIntent().getExtras()!=null){
            Bundle bundle = getIntent().getExtras();
            namaJasa.setText(bundle.getString("namaJasa"));
            deskripsi.setText(bundle.getString("deskripsi"));
            alamat.setText(bundle.getString("alamat"));
            noTelp.setText(bundle.getString("noTelp"));
            email.setText(bundle.getString("email"));
            latitude=bundle.getString("latitude");
            longitude=bundle.getString("longitude");
            if(bundle.getString("gambar")!=null) {
                StrGambar = bundle.getString("gambar");
                bitmap = StringToBitMap(StrGambar);
                ImageView imageView = (ImageView) findViewById(R.id.imageView);
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    public void loadImagefromGallery(View view) {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Detects request codes
        if(requestCode==GET_FROM_GALLERY && resultCode == TambahJasaActivity.RESULT_OK) {
            Uri selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                StrGambar = getStringImage(bitmap);
                ImageView imageView = (ImageView) findViewById(R.id.imageView);
                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }catch(Exception e){
            e.getMessage();
            return null;
        }
    }

    private void SimpanJasa() {
        Button btnSimpan = (Button) findViewById(R.id.btnSimpan);

        final EditText txtNamaJasa = (EditText) findViewById(R.id.namaJasa);
        final EditText txtAlamat = (EditText) findViewById(R.id.alamatJasa);
        final EditText txtDeskripsi = (EditText) findViewById(R.id.deskripsi);
        final EditText txtNoTelp = (EditText) findViewById(R.id.noTelp);
        final EditText txtEmail = (EditText) findViewById(R.id.email);


        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String member_id;
                final String namaJasa = txtNamaJasa.getText().toString();
                final String alamat = txtAlamat.getText().toString();
                final String noTelp = txtNoTelp.getText().toString();
                final String email = txtEmail.getText().toString();
                final String kategori = Integer.toString(cekKategori);
                final String deskripsi = txtDeskripsi.getText().toString();
                final String gambar = StrGambar;
                final String latitude1 = latitude;
                final String longitude1 = longitude;

                if(TextUtils.isEmpty(namaJasa) || TextUtils.isEmpty(alamat) || TextUtils.isEmpty(kategori) ||
                        TextUtils.isEmpty(deskripsi) || TextUtils.isEmpty(gambar) || TextUtils.isEmpty(latitude1) ||
                        TextUtils.isEmpty(longitude1) || TextUtils.isEmpty(noTelp) || TextUtils.isEmpty(email)){
                    builder.setMessage("Harap diisi");
                    builder.setNegativeButton("OK", null);
                    builder.show();
                }else {
                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            boolean error = false;
                            try {
                                JSONObject json = new JSONObject(response);
                                error = json.getBoolean("error");

                                if (!error) {
                                    Intent intent = new Intent(TambahJasaActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplication(),"berhasil",Toast.LENGTH_SHORT).show();
                                    finish();

                                } else {
                                    builder.setMessage("Gagal");
                                    builder.setNegativeButton("Retry", null);
                                    builder.show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                //Toast.makeText(getApplication(),"gagal",Toast.LENGTH_SHORT).show();
                            }

                        }
                    };
                    // AMBIL USER ID DARI SESSION
                    db = new SQLiteHandler(getApplicationContext());
                    session = new SessionManager(getApplicationContext());
                    HashMap<String, String> user = db.getUserDetails();
                    member_id = user.get("email");
                    TambahJasaRequest tambahRequest = new TambahJasaRequest(member_id, namaJasa, alamat, kategori, deskripsi,
                            gambar, latitude, longitude, noTelp, email, responseListener);
                    RequestQueue queue = Volley.newRequestQueue(TambahJasaActivity.this);
                    queue.add(tambahRequest);
                    startActivity(new Intent(getApplicationContext(), JasaSayaActivity.class));
                }
            }
        });
    }

    /**
     * Async task to get all food categories
     * */
    private void GetCategories(){
        Response.Listener<String> responseListener = new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                JSONObject jsonResponse = null;
                boolean error = false;
                try {
                    jsonResponse = new JSONObject(response);
                    error = jsonResponse.getBoolean("error");

                    if (!error){
                        JSONObject result = new JSONObject(response);
                        JSONArray array  = result.getJSONArray("kategori");

                        for(int i=0;i<array.length();i++) {
                            JSONObject catObj = array.getJSONObject(i);
                            int id = Integer.parseInt(catObj.getString("id"));
                            String kategor = catObj.getString("kategori");
                            Category cat = new Category(id, kategor);
                            listKategori.add(cat);
                        }
                        ArrayList<String> lables = new ArrayList<String>();

                        for (int i = 0; i < listKategori.size(); i++) {
                            lables.add(listKategori.get(i).getName());

                        }
                        spinner.setAdapter(new ArrayAdapter<String>(TambahJasaActivity.this, android.R.layout.simple_spinner_dropdown_item, lables));
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                    Toast.makeText(getApplication(), "Tidak",
                            Toast.LENGTH_LONG).show();
                }
            }
        };

        KategoriRequest kategoriRequest = new KategoriRequest(responseListener);
        RequestQueue queue = Volley.newRequestQueue(TambahJasaActivity.this);
        queue.add(kategoriRequest);

    }
}
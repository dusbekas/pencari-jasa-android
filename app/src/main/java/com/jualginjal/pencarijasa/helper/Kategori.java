package com.jualginjal.pencarijasa.helper;

/**
 * Created by arman on 3/28/17.
 */

public class Kategori {
    private String namaKategori;
    private String gambarKategori;
    private int idKategori;

    public int getIdKategori() {
        return idKategori;
    }

    public void setIdKategori(int idKategori) {
        this.idKategori = idKategori;
    }

    public String getNamaKategori() {
        return namaKategori;
    }

    public void setNamaKategori(String namaKategori) {
        this.namaKategori = namaKategori;
    }

    public String getGambarKategori() {
        return gambarKategori;
    }

    public void setGambarKategori(String gambarKategori) {
        this.gambarKategori = gambarKategori;
    }
}

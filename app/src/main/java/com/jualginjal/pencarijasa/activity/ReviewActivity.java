package com.jualginjal.pencarijasa.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.InstrumentationInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jualginjal.pencarijasa.R;
import com.jualginjal.pencarijasa.app.AppConfig;
import com.jualginjal.pencarijasa.helper.ReviewAdapter;
import com.jualginjal.pencarijasa.helper.ReviewModel;
import com.jualginjal.pencarijasa.helper.ReviewSumModel;
import com.jualginjal.pencarijasa.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.R.attr.data;

public class ReviewActivity extends AppCompatActivity {

    private GridLayoutManager gridLayoutManager;
    private ReviewAdapter adapter;
    private List<ReviewModel> data_list;

    private TextView txtRateAll;
    private TextView txtAchievement;
    private Button mRatingButton;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        //Get intent
        Bundle bundle = getIntent().getExtras();
        final int idJasa = Integer.parseInt(bundle.getString("jid"));

        load_review_summ(idJasa);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_review);
        data_list = new ArrayList<>();
        load_data_from_server(idJasa,0,1);

        gridLayoutManager = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new ReviewAdapter(this,data_list);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(gridLayoutManager.findLastCompletelyVisibleItemPosition()==data_list.size()-1){
                    load_data_from_server(idJasa,data_list.size(),1);
                }
            }
        });

        mRatingButton = (Button) findViewById(R.id.ratingButton);
        session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()) {
            mRatingButton.setVisibility(View.VISIBLE);
            mRatingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submitReview(idJasa);
                }
            });
        }else{
            mRatingButton.setVisibility(View.INVISIBLE);
        }
    }

    private void submitReview(int jid) {
        Intent intent = new Intent(ReviewActivity.this,ReviewSubmitActivity.class);
        intent.putExtra("jid", String.valueOf(jid));
        startActivity(intent);
    }


    private void load_data_from_server(final int id, final int start, final int offset) {
        AsyncTask<Integer, Void, Void> task  = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... integers) {
                OkHttpClient client = new OkHttpClient();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("id", String.valueOf(id))
                        .addFormDataPart("s", String.valueOf(start))
                        .addFormDataPart("o", String.valueOf(offset))
                        .build();

                Request request = new Request.Builder()
                        .url(AppConfig.URL_REVIEW)
                        .method("POST", RequestBody.create(null, new byte[0]))
                        .post(requestBody)
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                    JSONArray array = new JSONArray(response.body().string());

                    for(int i=0; i<array.length();i++){
                        JSONObject object = array.getJSONObject(i);
                        ReviewModel data = new ReviewModel(
                                object.getInt("jid"),
                                object.getInt("uid"),
                                object.getInt("rate"),
                                object.getString("review"),
                                object.getString("name")
                        );
                        data.setTgl(object.getString("tgl"));
                        data_list.add(data);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                adapter.notifyDataSetChanged();
            }
        };

        task.execute(id);
    }

    private void load_review_summ (final int id) {
        AsyncTask<Integer, Void, Void> task  = new AsyncTask<Integer, Void, Void>() {
            String strRateAll="", strAchievements="";

            @SuppressLint("DefaultLocale")
            @Override
            protected Void doInBackground(Integer... integers) {
                OkHttpClient client = new OkHttpClient();
                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("id", String.valueOf(id))
                        .build();

                Request request = new Request.Builder()
                        .url(AppConfig.URL_REVIEW_SUMM)
                        .method("POST", RequestBody.create(null, new byte[0]))
                        .post(requestBody)
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                    JSONArray array = new JSONArray(response.body().string());

                    JSONObject object = array.getJSONObject(0);
                    ReviewSumModel data = new ReviewSumModel(
                            object.getInt("jid"),
                            object.getInt("x1"),
                            object.getInt("x2"),
                            object.getInt("x3"),
                            object.getInt("x4"),
                            object.getInt("x5"),
                            (float) object.getDouble("arate")
                    );

                    strRateAll=String.format("%.1f",data.getArate());
                    strAchievements=(
                            "*1 = "+data.getX1()+"\n"+
                                    "*2 = "+data.getX2()+"\n"+
                                    "*3 = "+data.getX3()+"\n"+
                                    "*4 = "+data.getX4()+"\n"+
                                    "*5 = "+data.getX5()
                    );

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }

                return null;
            }

            @SuppressLint("SetTextI18n")
            @Override
            protected void onPostExecute(Void aVoid) {
                txtRateAll = (TextView) findViewById(R.id.txt_rate_overall);
                txtAchievement = (TextView) findViewById(R.id.txt_achievement);

                txtRateAll.setText("Score\n"+strRateAll);
                txtAchievement.setText(strAchievements);
            }

        };

        task.execute(id);
    }
}

package com.jualginjal.pencarijasa.helper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.jualginjal.pencarijasa.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.jualginjal.pencarijasa.R;

/**
 * Created by arman on 12/05/2017.
 */

public class RecyclerViewAdapterJasa extends RecyclerView.Adapter<RecyclerViewAdapterJasa.ViewHolder> {

    Context context;

    List<Jasa> getDataAdapter;

    ImageLoader imageLoader1;

    public RecyclerViewAdapterJasa(List<Jasa> getDataAdapter, Context context){

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.jasa_list_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Jasa getDataAdapter1 =  getDataAdapter.get(position);

        imageLoader1 = ServerImageParseAdapter.getInstance(context).getImageLoader();

        imageLoader1.get(getDataAdapter1.getGambarJasa(),
                ImageLoader.getImageListener(
                        holder.networkImageView,//Server Image
                        R.mipmap.ic_launcher,//Before loading server image the default showing image.
                        android.R.drawable.ic_dialog_alert //Error image if requested image dose not found on server.
                )
        );

        holder.networkImageView.setImageUrl(getDataAdapter1.getGambarJasa(), imageLoader1);
        holder.ImageTitleNameView.setText(getDataAdapter1.getNamaJasa());
        DecimalFormat REAL_FORMATTER = new DecimalFormat("0.##");
        holder.distanceView.setText(REAL_FORMATTER.format(getDataAdapter1.getDistance())+" km");
        holder.mScore.setText("Score = "+getDataAdapter1.getScore());
        //holder.jid = getDataAdapter1.getIdJasa();
    }


    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView ImageTitleNameView;
        public NetworkImageView networkImageView ;
        public TextView distanceView;
        public TextView mScore;
        //public int jid;


        public ViewHolder(View itemView) {

            super(itemView);

            ImageTitleNameView = (TextView) itemView.findViewById(R.id.txt_list_jasa) ;
            networkImageView = (NetworkImageView) itemView.findViewById(R.id.img_list_jasa) ;
            distanceView = (TextView)itemView.findViewById(R.id.txt_distance);
            mScore = (TextView)itemView.findViewById(R.id.txt_score);

        }
    }

    public void setFilter(List<Jasa> newList){
        getDataAdapter = new ArrayList<>();
        getDataAdapter.addAll(newList);
        notifyDataSetChanged();
    }
}
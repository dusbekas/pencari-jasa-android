package com.jualginjal.pencarijasa.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.jualginjal.pencarijasa.R;
import com.jualginjal.pencarijasa.app.AppConfig;
import com.jualginjal.pencarijasa.app.AppController;
import com.jualginjal.pencarijasa.helper.Jasa;
import com.jualginjal.pencarijasa.helper.Kategori;
import com.jualginjal.pencarijasa.helper.LocationDistance;
import com.jualginjal.pencarijasa.helper.RecyclerTouchListener;
import com.jualginjal.pencarijasa.helper.RecyclerViewAdapter;
import com.jualginjal.pencarijasa.helper.RecyclerViewAdapterJasa;
import com.jualginjal.pencarijasa.helper.RecyclerViewDivider;
import com.jualginjal.pencarijasa.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JasaKategoriActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private RecyclerView recyclerView;
    private static final String TAG = RegisterActivity.class.getSimpleName();

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    private double deviceLat;
    private double deviceLon;

    List<Jasa> GetDataAdapter;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    String JSON_IMAGE_TITLE_NAME = "service_name";
    String JSON_IMAGE_URL = "image_url";
    String JSON_SERVICE_ID = "sid";
    String JSON_SERVICE_LAT = "latitude";
    String JSON_SERVICE_LON = "longitude";
    String JSON_SERVICE_SCORE = "arate";
    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;


    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jasa_kategori);

        // First we need to check availability of play services
        if (checkPlayServices()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
        }

        //Get intent
        Bundle bundle = getIntent().getExtras();
        String idKategori = bundle.getString("idKategori");

        GetDataAdapter = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_jasa_kategori);
        recyclerView.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewlayoutManager);
        recyclerView.addItemDecoration(new RecyclerViewDivider(this));
        JSON_DATA_WEB_CALL(idKategori);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Jasa jasa = GetDataAdapter.get(position);
                //Toast.makeText(getApplicationContext(),String.valueOf(kategori.getIdKategori()), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(JasaKategoriActivity.this, DetailJasaActivity.class);
                intent.putExtra("idJasa",String.valueOf(jasa.getIdJasa()));
                startActivity(intent);

            }

            @Override
            public void onItemLongClick(View view, int position) {
                // ...
            }
        }));

        // Session manager
        session = new SessionManager(getApplicationContext());

        mDrawerList = (ListView)findViewById(R.id.nav_list_jasa);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout_jasa);
        mActivityTitle = getTitle().toString();

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            addLoggedinDrawerItems();
        } else {
            addDrawerItems();
        }

        //addDrawerItems();
        setupDrawer();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    public void JSON_DATA_WEB_CALL(final String category_id){
        Volley.newRequestQueue(JasaKategoriActivity.this).add(
                new CustomJsonRequest(Request.Method.POST, category_id, AppConfig.URL_SERVICE_CATEGORY,null,
                        new Response.Listener<JSONArray>(){

                            @Override
                            public void onResponse(JSONArray response) {
                                for (int i=0; i<response.length(); i++) {
                                    Jasa GetDataAdapter2 = new Jasa();

                                    try {
                                        JSONObject json = response.getJSONObject(i);
                                        GetDataAdapter2.setLatitude(json.getDouble(JSON_SERVICE_LAT));
                                        GetDataAdapter2.setLongitude(json.getDouble(JSON_SERVICE_LON));

                                        LocationDistance l = new LocationDistance();
                                        GetDataAdapter2.setDistance(l.locationDistance(deviceLat,deviceLon,GetDataAdapter2.getLatitude(),
                                                GetDataAdapter2.getLongitude()));
                                        GetDataAdapter2.setIdJasa(json.getInt(JSON_SERVICE_ID));
                                        GetDataAdapter2.setScore(json.getDouble(JSON_SERVICE_SCORE));
                                        GetDataAdapter2.setNamaJasa(json.getString(JSON_IMAGE_TITLE_NAME));
                                        GetDataAdapter2.setGambarJasa(AppConfig.IP_ADDRESS+json.getString(JSON_IMAGE_URL));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    GetDataAdapter.add(GetDataAdapter2);
                                }
                                recyclerViewadapter = new RecyclerViewAdapterJasa(GetDataAdapter, JasaKategoriActivity.this);
                                recyclerView.setAdapter(recyclerViewadapter);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Error: " + error.getMessage());
                    }
                }) {
                    @Override
                    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                        try {
                            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(
                                    response.headers));
                            return Response.success(new JSONArray(jsonString),HttpHeaderParser.parseCacheHeaders(
                                    response
                            ));
                        } catch (UnsupportedEncodingException e) {
                            return Response.error(new ParseError(e));
                        } catch (JSONException je) {
                            return Response.error(new ParseError(je));
                        }
                    }
                }

        );

    }

    private void addDrawerItems() {
        String[] arrayDrawer = getResources().getStringArray(R.array.nav_item_activity_titles);
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayDrawer);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(KategoriActivity.this, "Time for an upgrade!", Toast.LENGTH_SHORT).show();
                switch ((short)id) {
                    case 0: // HOME
                        startActivity(new Intent(getApplicationContext(), KategoriActivity.class));
                        finish();
                        break;
                    case 1: // CARI
                        startActivity(new Intent(getApplicationContext(), CariJasaActivity.class));
                        break;
                    case 2: // LOGIN
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        //finish();
                        break;
                    //drawer.closeDrawers();
                    //return true;
                    case 3: // REGISTER
                        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
                        //finish();
                        break;
                    //drawer.closeDrawers();
                    //return true;
                }

            }
        });
    }

    private void addLoggedinDrawerItems() {
        String[] arrayDrawer = getResources().getStringArray(R.array.nav_loggedin_item_activity_titles);
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayDrawer);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(KategoriActivity.this, "Time for an upgrade!", Toast.LENGTH_SHORT).show();
                switch ((short)id) {
                    case 0: // HOME
                        startActivity(new Intent(getApplicationContext(), KategoriActivity.class));
                        break;
                    case 1: // CARI
                        startActivity(new Intent(getApplicationContext(), CariJasaActivity.class));
                        break;
                    case 2: // MY SERVICE
                        startActivity(new Intent(getApplicationContext(), JasaSayaActivity.class));
                        //finish();
                        break;
                    //drawer.closeDrawers();
                    //return true;
                    case 3: // MY ACCOUNT
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        //finish();
                        break;
                    //drawer.closeDrawers();
                    //return true;
                }

            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle arg0) {
        displayLocation();
    }



    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPlayServices();
    }

    /**
     * Method to display the location on UI
     * */
    private void displayLocation() {
        try {
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
        } catch (SecurityException e){
            Toast.makeText(getApplicationContext(), "This device is not supported.",
                    Toast.LENGTH_LONG).show();
        }


        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();

            deviceLat = latitude;
            deviceLon = longitude;
//            Toast.makeText(getApplicationContext(), "We are in "+deviceLat+", "+deviceLon,
//                    Toast.LENGTH_LONG).show();

//            test.setText(deviceLat + " "+ deviceLon);

        } else {
            Toast.makeText(getApplicationContext(), "Error getting location.",
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    public class CustomJsonRequest extends Request {

        Map<String, String> params;
        private Response.Listener listener;

        public CustomJsonRequest(int requestMethod, String category_id, String url, Map<String, String> params,
                                 Response.Listener responseListener, Response.ErrorListener errorListener) {

            super(requestMethod, url, errorListener);


            params = new HashMap<String, String>();
            params.put("category_id", category_id);
            this.params = params;
            this.listener = responseListener;
        }

        @Override
        protected void deliverResponse(Object response) {
            listener.onResponse(response);

        }

        @Override
        public Map<String, String> getParams() throws AuthFailureError {

            return params;
        }

        @Override
        protected Response parseNetworkResponse(NetworkResponse response) {
            try {
                String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                return Response.success(new JSONObject(jsonString),
                        HttpHeaderParser.parseCacheHeaders(response));
            } catch (UnsupportedEncodingException e) {
                return Response.error(new ParseError(e));
            } catch (JSONException je) {
                return Response.error(new ParseError(je));
            }
        }
    }
}

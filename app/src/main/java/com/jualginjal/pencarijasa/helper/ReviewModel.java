package com.jualginjal.pencarijasa.helper;

/**
 * Created by nasrulhamid on 12/05/17.
 */

public class ReviewModel {
    private int jid, uid, rate;
    private String review, membername,tgl;

    public ReviewModel(int jid, int uid, int rate, String review, String membername) {

        this.jid = jid;
        this.uid = uid;
        this.rate = rate;
        this.review = review;
        this.membername = membername;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public int getJid() {
        return jid;
    }

    public void setJid(int jid) {
        this.jid = jid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }
}

package com.jualginjal.pencarijasa.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jualginjal.pencarijasa.R;
import com.jualginjal.pencarijasa.app.AppConfig;
import com.jualginjal.pencarijasa.helper.MySingleton;
import com.jualginjal.pencarijasa.helper.SQLiteHandler;
import com.jualginjal.pencarijasa.helper.ServerImageParseAdapter;
import com.jualginjal.pencarijasa.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class ReviewSubmitActivity extends AppCompatActivity {

    private EditText mReviewEditText;
    private RatingBar mReviewRatingBar;
    private Button mSubmitReviewButton;

    public static final String TAG = ReviewSubmitActivity.class.getSimpleName();
    private SessionManager session;
    AlertDialog.Builder dlgBuilder;

    String name, imageUrl;
    private TextView mNamaJasaTextView;
    private NetworkImageView mLogoImageView;
    protected ImageLoader imageLoader;
    private SQLiteHandler db;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_submit);

        mNamaJasaTextView = (TextView) findViewById(R.id.namaJasaTextView);
        mLogoImageView  = (NetworkImageView) findViewById(R.id.logoImageView);
        mReviewRatingBar = (RatingBar) findViewById(R.id.reviewRatingBar);
        mReviewEditText = (EditText) findViewById(R.id.reviewEditText);
        mSubmitReviewButton = (Button) findViewById(R.id.submitReviewButton);
        dlgBuilder = new AlertDialog.Builder(ReviewSubmitActivity.this);

        Bundle bundle = getIntent().getExtras();
        final String jid = bundle.getString("jid");

        // Session manager
        session = new SessionManager(getApplicationContext());
        //Toast.makeText(ReviewSubmitActivity.this,"JID="+jid,Toast.LENGTH_LONG);
        //Log.d(TAG, "onCreate: JID="+jid);

        getDetailJasa(jid);

        mSubmitReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
        public void onClick(View v) {
            // Check if user is already logged in or not
            //if (session.isLoggedIn()) {

                // AMBIL USER ID DARI SESSION
                db = new SQLiteHandler(getApplicationContext());
                session = new SessionManager(getApplicationContext());
                HashMap<String, String> user = db.getUserDetails();

                final String email = String.valueOf(user.get("email"));
                final String rate = String.valueOf(mReviewRatingBar.getRating());
                final String review = String.valueOf(mReviewEditText.getText());

                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_REVIEW_SUBMIT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            dlgBuilder.setTitle("Server Response");
                            dlgBuilder.setMessage("Response: "+response);
                            dlgBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mReviewEditText.setText("");
                                    mReviewRatingBar.setNumStars(0);
                                }
                            });
                            AlertDialog alertDialog = dlgBuilder.create();
                            alertDialog.show();
                        }
                    }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ReviewSubmitActivity.this,"Error", Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("jid", jid);
                        params.put("email", email);
                        params.put("rate", rate);
                        params.put("review", review);
//                            Log.d(TAG, "onCreate: jid="+jid+
//                                    " uid="+uid+
//                                    " rate="+rate+
//                                    " review="+review+
//                                    " URL="+AppConfig.URL_REVIEW_SUBMIT
//                            );
                        return params;
                    }
                };

                MySingleton.getInstance(ReviewSubmitActivity.this).addToRequestQueue(stringRequest);
                finish();

                Intent intent = new Intent(ReviewSubmitActivity.this, ReviewActivity.class);
                intent.putExtra("jid",jid);
                startActivity(intent);
            //}

            }
        });
    }

    private void getDetailJasa (String idJasa) {
        Volley.newRequestQueue(ReviewSubmitActivity.this).add(
                new DetailJasaActivity.CustomJsonRequest(Request.Method.POST, idJasa,
                        AppConfig.URL_SERVICE_DETAIL,null, new Response.Listener<JSONArray>(){


                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            JSONObject jasa = (JSONObject)response.get(0);

                            name = jasa.getString("service_name");
                            imageUrl = AppConfig.IP_ADDRESS + jasa.getString("image_url");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        mNamaJasaTextView.setText(name);

                        imageLoader = ServerImageParseAdapter.
                                getInstance(ReviewSubmitActivity.this.getApplicationContext()).getImageLoader();
                        imageLoader.get(imageUrl, ImageLoader.getImageListener(
                                mLogoImageView,R.mipmap.ic_launcher,android.R.drawable.ic_dialog_alert));
                        mLogoImageView.setImageUrl(imageUrl,imageLoader);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Error: " + error.getMessage());
                    }
                }) {
                    @Override
                    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                        try {
                            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(
                                    response.headers));
                            return Response.success(new JSONArray(jsonString),HttpHeaderParser.parseCacheHeaders(
                                    response
                            ));
                        } catch (UnsupportedEncodingException e) {
                            return Response.error(new ParseError(e));
                        } catch (JSONException je) {
                            return Response.error(new ParseError(je));
                        }
                    }
                }

        );
    }


}

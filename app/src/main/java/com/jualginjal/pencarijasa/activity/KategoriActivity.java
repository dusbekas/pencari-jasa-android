package com.jualginjal.pencarijasa.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.jualginjal.pencarijasa.R;
import com.jualginjal.pencarijasa.app.AppConfig;
import com.jualginjal.pencarijasa.helper.Kategori;
import com.jualginjal.pencarijasa.helper.RecyclerTouchListener;
import com.jualginjal.pencarijasa.helper.RecyclerViewAdapter;
import com.jualginjal.pencarijasa.helper.RecyclerViewDivider;
import com.jualginjal.pencarijasa.helper.SQLiteHandler;
import com.jualginjal.pencarijasa.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class KategoriActivity extends AppCompatActivity {

    //private List<Kategori> kategoriList = new ArrayList<>();
    private RecyclerView recyclerView;
    //private KategoriAdapter kategoriAdapter;

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private DividerItemDecoration dividerItemDecoration;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;

    List<Kategori> GetDataAdapter;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    String GET_JSON_DATA_HTTP_URL = AppConfig.URL_CATEGORY;
    String JSON_IMAGE_TITLE_NAME = "category";
    String JSON_IMAGE_URL = "image_url";
    String JSON_CATEGORY_ID = "cid";
    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;


    private SessionManager session;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategori);

        GetDataAdapter = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_kategori);
        recyclerView.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewlayoutManager);
        recyclerView.addItemDecoration(new RecyclerViewDivider(this));
        JSON_DATA_WEB_CALL();


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Kategori kategori = GetDataAdapter.get(position);
                //Toast.makeText(getApplicationContext(),String.valueOf(kategori.getIdKategori()), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(KategoriActivity.this, JasaKategoriActivity.class);
                intent.putExtra("idKategori",String.valueOf(kategori.getIdKategori()));
                startActivity(intent);

            }

            @Override
            public void onItemLongClick(View view, int position) {
                // ...
            }
        }));


        // Session manager
        session = new SessionManager(getApplicationContext());

        mDrawerList = (ListView)findViewById(R.id.nav_list);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // AMBIL USER ID DARI SESSION
//            db = new SQLiteHandler(getApplicationContext());
//            session = new SessionManager(getApplicationContext());
//            HashMap<String, String> user = db.getUserDetails();
//            Toast.makeText(getApplicationContext(),"EMAIL="+user.get("email"), Toast.LENGTH_SHORT).show();
            addLoggedinDrawerItems();
        } else {
            addDrawerItems();
        }

        //addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    public void JSON_DATA_WEB_CALL(){
        jsonArrayRequest = new JsonArrayRequest(GET_JSON_DATA_HTTP_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){
        for(int i = 0; i<array.length(); i++) {
            Kategori GetDataAdapter2 = new Kategori();
            JSONObject json = null;

            try {
                json = array.getJSONObject(i);
                GetDataAdapter2.setIdKategori(json.getInt(JSON_CATEGORY_ID));
                GetDataAdapter2.setNamaKategori(json.getString(JSON_IMAGE_TITLE_NAME));
                GetDataAdapter2.setGambarKategori(AppConfig.IP_ADDRESS+json.getString(JSON_IMAGE_URL));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            GetDataAdapter.add(GetDataAdapter2);
        }

        recyclerViewadapter = new RecyclerViewAdapter(GetDataAdapter, this);
        recyclerView.setAdapter(recyclerViewadapter);
    }



    private void addDrawerItems() {
        String[] arrayDrawer = getResources().getStringArray(R.array.nav_item_activity_titles);
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayDrawer);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(KategoriActivity.this, "Time for an upgrade!", Toast.LENGTH_SHORT).show();
                switch ((short)id) {
                    case 0: // HOME
                        startActivity(new Intent(KategoriActivity.this, KategoriActivity.class));
                        finish();
                        break;
                    case 1: // CARI
                        startActivity(new Intent(KategoriActivity.this, CariJasaActivity.class));
                        break;
                    case 2: // LOGIN
                        startActivity(new Intent(KategoriActivity.this, LoginActivity.class));
                        //finish();
                        break;
                    //drawer.closeDrawers();
                    //return true;
                    case 3: // REGISTER
                        startActivity(new Intent(KategoriActivity.this, RegisterActivity.class));
                        //finish();
                        break;
                    //drawer.closeDrawers();
                    //return true;
                }

            }
        });
    }

    private void addLoggedinDrawerItems() {
        String[] arrayDrawer = getResources().getStringArray(R.array.nav_loggedin_item_activity_titles);
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayDrawer);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(KategoriActivity.this, "Time for an upgrade!", Toast.LENGTH_SHORT).show();
                switch ((short)id) {
                    case 0: // HOME
                        startActivity(new Intent(KategoriActivity.this, KategoriActivity.class));
                        break;
                    case 1: // CARI
                        startActivity(new Intent(KategoriActivity.this, CariJasaActivity.class));
                        break;
                    case 2: // MY SERVICE
                        startActivity(new Intent(KategoriActivity.this, JasaSayaActivity.class));
                        //finish();
                        break;
                    //drawer.closeDrawers();
                    //return true;
                    case 3: // MY ACCOUNT
                        startActivity(new Intent(KategoriActivity.this, MainActivity.class));
                        //finish();
                        break;
                    //drawer.closeDrawers();
                    //return true;
                }

            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
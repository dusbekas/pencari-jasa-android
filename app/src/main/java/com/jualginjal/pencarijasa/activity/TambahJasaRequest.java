package com.jualginjal.pencarijasa.activity;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;
import com.jualginjal.pencarijasa.app.AppConfig;
import static com.jualginjal.pencarijasa.app.AppConfig.URL_TAMBAHJASA;

/**
 * Created by Hut Hut on 5/3/2017.
 */

public class TambahJasaRequest extends StringRequest {

    private Map<String, String> params;

    public TambahJasaRequest(String member_id, String namaJasa, String alamat, String kategori, String deskripsi,
                             String gambar, String latitude, String longitude, String noTelp, String email, Response.Listener<String> listener){
        super(Method.POST,URL_TAMBAHJASA, listener, null);
        params = new HashMap<>();
        params.put("member_id", member_id);
        params.put("namaJasa", namaJasa);
        params.put("alamat", alamat);
        params.put("kategori", kategori);
        params.put("deskripsi", deskripsi);
        params.put("gambar", gambar);
        params.put("latitude", latitude);
        params.put("longitude", longitude);
        params.put("noTelp", noTelp);
        params.put("email", email);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
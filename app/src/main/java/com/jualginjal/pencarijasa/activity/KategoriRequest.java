package com.jualginjal.pencarijasa.activity;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.jualginjal.pencarijasa.app.AppConfig;

import static com.jualginjal.pencarijasa.app.AppConfig.URL_KATEGORI;


/**
 * Created by Hut Hut on 5/2/2017.
 */

public class KategoriRequest extends StringRequest {

    public KategoriRequest(Response.Listener<String> listener){
        super(Method.POST,URL_KATEGORI, listener, null);
    }
}
package com.jualginjal.pencarijasa.helper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jualginjal.pencarijasa.R;

import java.util.List;
/**
 * Created by nasrulhamid on 12/05/17.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {
    private Context context;
    private List<ReviewModel> my_data;

    public ReviewAdapter(Context context, List<ReviewModel> my_data) {
        this.context = context;
        this.my_data = my_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_review,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReviewAdapter.ViewHolder holder, int position) {
        holder.review.setText(my_data.get(position).getReview());
        holder.member_name.setText(my_data.get(position).getMembername());
        holder.userRateBar.setIsIndicator(true);
        holder.userRateBar.setRating(my_data.get(position).getRate());
        holder.mTglReview.setText(my_data.get(position).getTgl());
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView review, member_name, rate, mTglReview;
        public RatingBar userRateBar;

        public ViewHolder(View itemView) {
            super(itemView);
            review = (TextView) itemView.findViewById(R.id.txt_review);
            member_name = (TextView) itemView.findViewById(R.id.txt_membername);
            userRateBar = (RatingBar) itemView.findViewById(R.id.userRatingBar);
            mTglReview = (TextView) itemView.findViewById(R.id.txt_tglReview);
        }
    }
}

package com.jualginjal.pencarijasa.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
//import android.widget.SearchView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.jualginjal.pencarijasa.R;
import com.jualginjal.pencarijasa.app.AppConfig;
import com.jualginjal.pencarijasa.helper.Jasa;
import com.jualginjal.pencarijasa.helper.LocationDistance;
import com.jualginjal.pencarijasa.helper.RecyclerTouchListener;
import com.jualginjal.pencarijasa.helper.RecyclerViewAdapter;
import com.jualginjal.pencarijasa.helper.RecyclerViewAdapterJasa;
import com.jualginjal.pencarijasa.helper.RecyclerViewDivider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class CariJasaActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private MenuItem searchMenuItem;
    private SearchView searchView;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private int sortValue;
    private double deviceLat;
    private double deviceLon;

    private RecyclerView recyclerView;
    List<Jasa> GetDataAdapter;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerViewAdapterJasa recyclerViewadapter;
    String JSON_IMAGE_TITLE_NAME = "service_name";
    String JSON_IMAGE_URL = "image_url";
    String JSON_SERVICE_ID = "sid";
    String JSON_SERVICE_LAT = "latitude";
    String JSON_SERVICE_LON = "longitude";
    String JSON_SERVICE_SCORE = "arate";
    Toolbar toolbar;
    Button mButtonSort;
    String strCari="";
    int inSort;
    private List<String> mLiFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_jasa);

        // First we need to check availability of play services
        if (checkPlayServices()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        GetDataAdapter = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.rv_list_jasa);
        recyclerView.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewlayoutManager);
        recyclerView.addItemDecoration(new RecyclerViewDivider(this));

        JSON_DATA_WEB_CALL(strCari);

        recyclerView.addOnItemTouchListener(
            new RecyclerTouchListener(
                getApplicationContext(),
                recyclerView,
                new RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Jasa jasa = GetDataAdapter.get(position);

                        Intent intent = new Intent(CariJasaActivity.this, DetailJasaActivity.class);
                        intent.putExtra("idJasa",String.valueOf(jasa.getIdJasa()));
                        startActivity(intent);

                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                        // ...
                    }
                }
            )
        );

        mButtonSort = (Button) findViewById(R.id.btnSort);
        mButtonSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu mPopupMenu = new PopupMenu(CariJasaActivity.this,mButtonSort);
                mPopupMenu.getMenuInflater().inflate(R.menu.menu_sorting,mPopupMenu.getMenu());

                mPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        if(menuItem.getTitle().toString().toLowerCase().equals("lokasi terdekat"))inSort=1;
                        else if(menuItem.getTitle().toString().toLowerCase().equals("nilai ulasan"))inSort=2;
                        JSON_DATA_WEB_CALL(strCari);
                        GetDataAdapter.clear();
                        recyclerViewadapter.notifyDataSetChanged();
                        return true;
                    }
                });
                mPopupMenu.show();
            }
        });

        mLiFilter = new ArrayList<>();



    }

    public void JSON_DATA_WEB_CALL(String strQuery){
        Volley.newRequestQueue(CariJasaActivity.this).add(
                new CariJasaActivity.CustomJsonRequest(Request.Method.POST, strQuery, AppConfig.URL_SERVICE_CARI,null,
                        new Response.Listener<JSONArray>(){

                            @Override
                            public void onResponse(JSONArray response) {
                                for (int i=0; i<response.length(); i++) {
                                    Jasa GetDataAdapter2 = new Jasa();

                                    try {
                                        JSONObject json = response.getJSONObject(i);
                                        GetDataAdapter2.setLatitude(json.getDouble(JSON_SERVICE_LAT));
                                        GetDataAdapter2.setLongitude(json.getDouble(JSON_SERVICE_LON));

                                        LocationDistance l = new LocationDistance();
                                        GetDataAdapter2.setDistance(l.locationDistance(deviceLat,deviceLon,GetDataAdapter2.getLatitude(),
                                                GetDataAdapter2.getLongitude()));
                                        GetDataAdapter2.setIdJasa(json.getInt(JSON_SERVICE_ID));
                                        GetDataAdapter2.setScore(json.getDouble(JSON_SERVICE_SCORE));
                                        GetDataAdapter2.setNamaJasa(json.getString(JSON_IMAGE_TITLE_NAME));
                                        GetDataAdapter2.setGambarJasa(AppConfig.IP_ADDRESS+json.getString(JSON_IMAGE_URL));

                                        /*
                                            Sorting data
                                         */
                                        if (inSort==1){
                                            Collections.sort(GetDataAdapter, new Comparator<Jasa>() {
                                                @Override
                                                public int compare(Jasa o1, Jasa o2) {
                                                    return String.valueOf(o1.getDistance()).compareTo(String.valueOf(
                                                            o2.getDistance()));
                                                }
                                            });
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    GetDataAdapter.add(GetDataAdapter2);
                                }
                                recyclerViewadapter = new RecyclerViewAdapterJasa(GetDataAdapter, CariJasaActivity.this);
                                recyclerView.setAdapter(recyclerViewadapter);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Error: " + error.getMessage());
                    }
                }) {
                    @Override
                    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                        try {
                            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(
                                    response.headers));
                            return Response.success(new JSONArray(jsonString),HttpHeaderParser.parseCacheHeaders(
                                    response
                            ));
                        } catch (UnsupportedEncodingException e) {
                            return Response.error(new ParseError(e));
                        } catch (JSONException je) {
                            return Response.error(new ParseError(je));
                        }
                    }
                }

        );

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items,menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        strCari=query;
        JSON_DATA_WEB_CALL(strCari);
//        Log.d(TAG, "onQueryTextSubmit: "+query);
        GetDataAdapter.clear();
        recyclerViewadapter.notifyDataSetChanged();

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        strCari=newText;
        JSON_DATA_WEB_CALL(strCari);
//        Log.d(TAG, "onQueryTextSubmit: "+query);
        GetDataAdapter.clear();
        recyclerViewadapter.notifyDataSetChanged();

        return false;
    }

    @Override
    public void onConnected(Bundle arg0) {
        displayLocation();
    }



    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPlayServices();
    }

    /**
     * Method to display the location on UI
     * */
    private void displayLocation() {
        try {
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
        } catch (SecurityException e){
            Toast.makeText(getApplicationContext(), "This device is not supported.",
                    Toast.LENGTH_LONG).show();
        }


        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();

            deviceLat = latitude;
            deviceLon = longitude;
//            Toast.makeText(getApplicationContext(), "We are in "+deviceLat+", "+deviceLon,
//                    Toast.LENGTH_LONG).show();

//            test.setText(deviceLat + " "+ deviceLon);

        } else {
            Toast.makeText(getApplicationContext(), "Error getting location.",
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    public class CustomJsonRequest extends Request {

        Map<String, String> params;
        private Response.Listener listener;

        public CustomJsonRequest(int requestMethod, String strQuery,  String url, Map<String, String> params,
                                 Response.Listener responseListener, Response.ErrorListener errorListener) {

            super(requestMethod, url, errorListener);


            params = new HashMap<String, String>();
            params.put("strQuery", strQuery);
            params.put("strOrder", String.valueOf(inSort));
            this.params = params;
            this.listener = responseListener;
        }

        @Override
        protected void deliverResponse(Object response) {
            listener.onResponse(response);

        }

        @Override
        public Map<String, String> getParams() throws AuthFailureError {

            return params;
        }

        @Override
        protected Response parseNetworkResponse(NetworkResponse response) {
            try {
                String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                return Response.success(new JSONObject(jsonString),
                        HttpHeaderParser.parseCacheHeaders(response));
            } catch (UnsupportedEncodingException e) {
                return Response.error(new ParseError(e));
            } catch (JSONException je) {
                return Response.error(new ParseError(je));
            }
        }
    }
}
